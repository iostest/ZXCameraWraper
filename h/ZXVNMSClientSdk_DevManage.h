//////////////////////////////////////////////////////////////////////
// ZXVNMSClientSdk_DevManage.h
//////////////////////////////////////////////////////////////////////
#if !defined(_ZXVNMS_CLIENT_SDK_DEVMANAGE_H_INCLUDED_)
#define _ZXVNMS_CLIENT_SDK_DEVMANAGE_H_INCLUDED_

#ifdef WIN32
	#ifdef ZXVNMSSDK_EXPORTS
		#define ZXVNMSSDK_EXPORT __declspec(dllexport)
	#else
		#define ZXVNMSSDK_EXPORT __declspec(dllimport)
	#endif
#else
	#define ZXVNMSSDK_EXPORT
#endif

#include "ZXVNMSClientSdk_DataType.h"

/**
 * @brief 局站创建
 *
 * @param[in] pTOffice	局站结构指针
 *
 * @return 0	成功
 * @return <0	失败
 */
extern "C" ZXVNMSSDK_EXPORT int __stdcall ZXVNMS_DEVMGR_CreateOffice(ZXVNMS_LPTOffice pTOffice);

/**
 * @brief 局站修改
 *
 * @param[in] pTOffice	局站结构指针
 *
 * @return 0	成功
 * @return <0	失败
 */
extern "C" ZXVNMSSDK_EXPORT int __stdcall ZXVNMS_DEVMGR_UpdateOffice(ZXVNMS_LPTOffice pTOffice);

/**
 * @brief 局站删除
 *
 * @param[in]	office_id	局站ID
 *
 * @return 0	成功
 * @return <0	失败
 */
extern "C" ZXVNMSSDK_EXPORT int __stdcall ZXVNMS_DEVMGR_DeleteOffice(int office_id);

/**
 * @brief 解码器创建
 *
 * @param[in] pTDecoder	解码器结构指针
 *
 * @return 0	成功
 * @return <0	失败
 */
extern "C" ZXVNMSSDK_EXPORT int __stdcall ZXVNMS_DEVMGR_CreateDecoder(ZXVNMS_LPTDecoder pTDecoder);

/**
 * @brief 解码器修改
 *
 * @param[in] pTDecoder	解码器结构指针
 *
 * @return 0	成功
 * @return <0	失败
 */
extern "C" ZXVNMSSDK_EXPORT int __stdcall ZXVNMS_DEVMGR_UpdateDecoder(ZXVNMS_LPTDecoder pTDecoder);

/**
 * @brief 解码器删除
 *
 * @param[in]	device_id	解码器ID
 *
 * @return 0	成功
 * @return <0	失败
 */
extern "C" ZXVNMSSDK_EXPORT int __stdcall ZXVNMS_DEVMGR_DeleteDecoder(const char* device_id);

/**
 * @brief 编码器创建
 *
 * @param[in] pTEncoder	编码器结构指针
 *
 * @return 0	成功
 * @return <0	失败
 */
extern "C" ZXVNMSSDK_EXPORT int __stdcall ZXVNMS_DEVMGR_CreateEncoder(ZXVNMS_LPTEncoder pTEncoder);

/**
 * @brief 编码器修改
 *
 * @param[in] pTEncoder	编码器结构指针
 *
 * @return 0	成功
 * @return <0	失败
 */
extern "C" ZXVNMSSDK_EXPORT int __stdcall ZXVNMS_DEVMGR_UpdateEncoder(ZXVNMS_LPTEncoder pTEncoder);

/**
 * @brief 编码器删除
 *
 * @param[in]	device_id	编码器ID
 *
 * @return 0	成功
 * @return <0	失败
 */
extern "C" ZXVNMSSDK_EXPORT int __stdcall ZXVNMS_DEVMGR_DeleteEncoder(const char* device_id);

/**
 * @brief 摄像头创建
 *
 * @param[in] pTCamera	摄像头结构指针
 *
 * @return 0	成功
 * @return <0	失败
 */
extern "C" ZXVNMSSDK_EXPORT int __stdcall ZXVNMS_DEVMGR_CreateCamera(ZXVNMS_LPTCamera pTCamera);

/**
 * @brief 摄像头修改
 *
 * @param[in] pTCamera	摄像头结构指针
 *
 * @return 0	成功
 * @return <0	失败
 */
extern "C" ZXVNMSSDK_EXPORT int __stdcall ZXVNMS_DEVMGR_UpdateCamera(ZXVNMS_LPTCamera pTCamera);

/**
 * @brief 摄像头删除
 *
 * @param[in]	device_id	摄像头ID
 *
 * @return 0	成功
 * @return <0	失败
 */
extern "C" ZXVNMSSDK_EXPORT int __stdcall ZXVNMS_DEVMGR_DeleteCamera(const char* device_id);

/**
 * @brief DI创建
 *
 * @param[in] pTDI	DI结构指针
 *
 * @return 0	成功
 * @return <0	失败
 */
extern "C" ZXVNMSSDK_EXPORT int __stdcall ZXVNMS_DEVMGR_CreateDI(ZXVNMS_LPTDI pTDI);

/**
 * @brief DI修改
 *
 * @param[in] pTDI	DI结构指针
 *
 * @return 0	成功
 * @return <0	失败
 */
extern "C" ZXVNMSSDK_EXPORT int __stdcall ZXVNMS_DEVMGR_UpdateDI(ZXVNMS_LPTDI pTDI);

/**
 * @brief DI删除
 *
 * @param[in]	device_id	DI ID
 *
 * @return 0	成功
 * @return <0	失败
 */
extern "C" ZXVNMSSDK_EXPORT int __stdcall ZXVNMS_DEVMGR_DeleteDI(const char* device_id);

/**
 * @brief DO创建
 *
 * @param[in] pTDO	DO结构指针
 *
 * @return 0	成功
 * @return <0	失败
 */
extern "C" ZXVNMSSDK_EXPORT int __stdcall ZXVNMS_DEVMGR_CreateDO(ZXVNMS_LPTDO pTDO);

/**
 * @brief DO修改
 *
 * @param[in] pTDO	DO结构指针
 *
 * @return 0	成功
 * @return <0	失败
 */
extern "C" ZXVNMSSDK_EXPORT int __stdcall ZXVNMS_DEVMGR_UpdateDO(ZXVNMS_LPTDO pTDO);

/**
 * @brief DO删除
 *
 * @param[in]	device_id	DO ID
 *
 * @return 0	成功
 * @return <0	失败
 */
extern "C" ZXVNMSSDK_EXPORT int __stdcall ZXVNMS_DEVMGR_DeleteDO(const char* device_id);

/**
 * @brief 预置位创建
 *
 * @param[in] pTPreset 预置位结构指针
 *
 * @return 0	成功
 * @return <0	失败
 */
extern "C" ZXVNMSSDK_EXPORT int __stdcall ZXVNMS_DEVMGR_CreatePreset(ZXVNMS_LPTPreset pTPreset);

/**
 * @brief 预置位修改
 *
 * @param[in] pTPreset 预置位结构指针
 *
 * @return 0	成功
 * @return <0	失败
 */
extern "C" ZXVNMSSDK_EXPORT int __stdcall ZXVNMS_DEVMGR_UpdatePreset(ZXVNMS_LPTPreset pTPreset);

/**
 * @brief 预置位删除
 *
 * @param[in] pTPreset 预置位结构指针
 *
 * @return 0	成功
 * @return <0	失败
 */
extern "C" ZXVNMSSDK_EXPORT int __stdcall ZXVNMS_DEVMGR_DeletePreset(ZXVNMS_LPTPreset pTPreset);

/**
* @brief 告警联动创建
*
* @param[in] pTAlarmLinkage 告警联动结构指针
*
* @return 0	成功
* @return <0	失败
*/
extern "C" ZXVNMSSDK_EXPORT int __stdcall ZXVNMS_DEVMGR_CreateAlarmLinkage(ZXVNMS_LPTAlarmLinkage pTAlarmLinkage);

/**
* @brief 告警联动删除
*
* @param[in] pTAlarmLinkage 告警联动结构指针
*
* @return 0	成功
* @return <0	失败
*/
extern "C" ZXVNMSSDK_EXPORT int __stdcall ZXVNMS_DEVMGR_DeleteAlarmLinkage(ZXVNMS_LPTAlarmLinkage pTAlarmLinkage);

/**
* @brief 根据告警设备ID删除告警联动
*
* @param[in] alarm_device_id 告警联动结构指针
*
* @return 0	成功
* @return <0	失败
*/
extern "C" ZXVNMSSDK_EXPORT int __stdcall ZXVNMS_DEVMGR_DeleteAlarmLinkageByAlarmDevice(const char* alarm_device_id);

/**
 * @brief 设置告警接收回调函数，设置成功后，登录成功时会调用回调函数上报所有数据库中现有告警，
 * 当告警产生或消除时调用回调函数
 *
 * @param[in] fAlarmCallback 回调函数指针
 * 回调函数参数
 *		param[in] handle ZXVNMS_InitSession 返回的handle
 *		param[in] pTAlarm 告警结构指针
 *		param[in] pUser 用户参数
 * @param[in] pUser 用户参数
 *
 * @return 0 成功
 * @return <0失败
 */
extern "C" ZXVNMSSDK_EXPORT int __stdcall ZXVNMS_SetAlarmCallback(
	void(__stdcall *fAlarmCallback)(int handle,ZXVNMS_LPTAlarm pTAlarm,void* pUser),void* pUser);

/**
 * @brief 设置异常接收回调函数，设置成功后，登录成功时会调用回调函数上报所有数据库中现有异常，
 * 当异常产生或消除时调用回调函数
 *
 * @param[in] fExceptionCallback 回调函数指针
 * 回调函数参数
 *		param[in] handle ZXVNMS_InitSession 返回的handle
 *		param[in] pTException 异常结构指针
 *		param[in] pUser 用户参数
 * @param[in] pUser 用户参数
 *
 * @return 0 成功
 * @return <0失败
 */
extern "C" ZXVNMSSDK_EXPORT int __stdcall ZXVNMS_SetExceptionCallback(
	void(__stdcall *fExceptionCallback)(int handle,ZXVNMS_LPTException pTException,void* pUser),void* pUser);

/**
 * @brief 获取图像参数
 *
 * @param[in/out] lpTPicParam 图像参数结构指针，
 * 获取时填入encoder_id,channel,
 * 获取成功后可以取得其他属性
 *
 * @return 0 成功
 * @return <0失败
 */
extern "C" ZXVNMSSDK_EXPORT int __stdcall ZXVNMS_GetPicParam(ZXVNMS_LPTPicParam lpTPicParam);

/**
 * @brief 设置图像参数
 *
 * @param[in] lpTPicParam 图像参数结构指针，设置参数指定编码器、通道的图像参数
 *
 * @return 0 成功
 * @return <0失败
 */
extern "C" ZXVNMSSDK_EXPORT int __stdcall ZXVNMS_SetPicParam(ZXVNMS_LPTPicParam lpTPicParam);

/**
 * @brief 获取OSD
 *
 * @param[in/out] lpTOSD OSD结构指针，
 * 获取时填入encoder_id,channel,
 * 获取成功后可以取得其他属性
 *
 * @return 0 成功
 * @return <0失败
 */
extern "C" ZXVNMSSDK_EXPORT int __stdcall ZXVNMS_GetOSD(ZXVNMS_LPTOSD lpTOSD);

/**
 * @brief 设置OSD
 *
 * @param[in] lpTOSD OSD结构指针，设置参数指定编码器、通道的OSD
 *
 * @return 0 成功
 * @return <0失败
 */
extern "C" ZXVNMSSDK_EXPORT int __stdcall ZXVNMS_SetOSD(ZXVNMS_LPTOSD lpTOSD);

/**
 * @brief 获取告警布防
 *
 * @param[in/out] lpTAlarmDeploy TAlarmDeploy结构指针，
 * 获取时填入encoder_id,channel,alarmtype,
 * 获取成功后可以取得其他属性
 *
 * @return 0 成功
 * @return <0失败
 */
extern "C" ZXVNMSSDK_EXPORT int __stdcall ZXVNMS_GetAlarmDeploy(ZXVNMS_LPTAlarmDeploy lpTAlarmDeploy);

/**
 * @brief 设置告警布防
 *
 * @param[in] lpTAlarmDeploy TAlarmDeploy结构指针，
 * 设置encoder_id,channel,alarmtype指定的告警布防
 *
 * @return 0 成功
 * @return <0失败
 */
extern "C" ZXVNMSSDK_EXPORT int __stdcall ZXVNMS_SetAlarmDeploy(ZXVNMS_LPTAlarmDeploy lpTAlarmDeploy);

/**
 * @brief 获取码流参数
 * 
 * @param[out] lpEncodeStream 码流参数结构指针
 * 通过设置摄像头的编码器id、stream_kind、port来获取对应的码流参数
 * 
 * @return 0 成功
 * @return <0 失败
 */
extern "C" ZXVNMSSDK_EXPORT int __stdcall ZXVNMS_GetEncodeStream(ZXVNMS_LPTEncodeStream lpTEncodeStream);

/**
 * @brief 设置码流参数
 * 
 * @param[in] lpEncodeStream 码流参数结构指针
 * 
 * @return 0 成功
 * @return <0 失败
 */
extern "C" ZXVNMSSDK_EXPORT int __stdcall ZXVNMS_SetEncodeStream(ZXVNMS_LPTEncodeStream lpTEncodeStream);

/**
 * @brief 获取前置录像参数
 * 
 * @param[out] lpPuStorageTask 前置录像结构指针
 * 通过设定
 *   SIPHead的From: 用户编号
 *   SIPHead的To: PU设备编号
 *   视频通道号videoId
 * 来获取参数
 * 
 * @return 0 成功
 * @return <0 失败
 */
extern "C" ZXVNMSSDK_EXPORT int __stdcall ZXVNMS_GetPULocalStorageTask(ZXVNMS_LPTPUStorageTask lpTPuStorageTask);

/**
 * @brief 设置前置录像参数
 * 
 * @param[in] lpPuStorageTask 前置录像结构指针
 * 
 * @return 0 成功
 * @return <0 失败
 */
extern "C" ZXVNMSSDK_EXPORT int __stdcall ZXVNMS_SetPULocalStorageTask(ZXVNMS_LPTPUStorageTask lpTPuStorageTask);

/**
 * @brief 获取录像存储配置参数
 * 
 * @param[out] lpVideoStorageConfig 录像存储配置参数结构指针
 * 通过设定encoder_id、outport和bak_type来获取参数
 * 
 * @return 0 成功
 * @return <0 失败
 */
extern "C" ZXVNMSSDK_EXPORT int __stdcall ZXVNMS_GetVideoStorageConfig(ZXVNMS_LPTVideoStorageConfig lpTVideoStorageConfig);

/**
 * @brief 设置录像存储配置参数
 * 
 * @param[in] lpVideoStorageConfig 录像存储配置参数结构指针
 * 
 * @return 0 成功
 * @return <0 失败
 */
extern "C" ZXVNMSSDK_EXPORT int __stdcall ZXVNMS_SetVideoStorageConfig(ZXVNMS_LPTVideoStorageConfig lpTVideoStorageConfig);

/**
 * @brief 控制DO
 * 
 * @param[in] encoder_id 编码器ID
 * @param[in] do_port DO通道号 1，2，...
 * @param[in] bOpen true：打开 false：关闭
 * 
 * @return 0 成功
 * @return <0 失败
 */
extern "C" ZXVNMSSDK_EXPORT int __stdcall ZXVNMS_ControlDO(
	const char* encoder_id,
	int do_port,
	bool bOpen);

/**
 * @brief 重启编码器
 * 
 * @param[in] encoder_id 编码器ID
 * 
 * @return 0 成功
 * @return <0 失败
 */
extern "C" ZXVNMSSDK_EXPORT int __stdcall ZXVNMS_RebootEncoder(const char* encoder_id);

/**
 * @brief 清空编码器密码
 * 
 * @param[in] encoder_id 编码器ID
 * 
 * @return 0 成功
 * @return <0 失败
 */
extern "C" ZXVNMSSDK_EXPORT int __stdcall ZXVNMS_ClearEncoderPassword(const char* encoder_id);

/**
 * @brief 获取编码器输入输出通道数
 * 
 * @param[in] encoder_id 编码器ID
 * @param[out] dicount DI数
 * @param[out] docount DO数
 * @param[out] videocount 视频通道数
 * @param[out] lpTChannelNum DI、DO、视频通道号
 * 
 * @return 0 成功
 * @return <0 失败
 */
extern "C" ZXVNMSSDK_EXPORT int __stdcall ZXVNMS_GetEncoderInOutNum(
	const char* encoder_id,
	int* dicount,
	int* docount,
	int* videocount,
	ZXVNMS_LPTChannelNum lpTChannelNum);

/**
 * @brief 获取编码器DI告警配置参数
 * 
 * @param[in/out] lpTEncoderDIAlarm
 *	获取时填入encoder_id,diport,
 *	获取成功后可以取得其他属性
 *
 * @return 0 成功
 * @return <0 失败
 */
extern "C" ZXVNMSSDK_EXPORT int __stdcall ZXVNMS_GetEncoderDIAlarm(ZXVNMS_LPTEncoderDIAlarm lpTEncoderDIAlarm);

/**
 * @brief 设置编码器DI告警配置参数
 * 
 * @param[in] lpTEncoderDIAlarm
 *
 * @return 0 成功
 * @return <0 失败
 */
extern "C" ZXVNMSSDK_EXPORT int __stdcall ZXVNMS_SetEncoderDIAlarm(ZXVNMS_LPTEncoderDIAlarm lpTEncoderDIAlarm);

/**
 * @brief 设置DI布、撤防状态接收回调函数，若设置成功，DI布、撤防状态发生改变或调用ZXVNMS_RequestEncoderDIArmingState接口时，
 * 调用回调函数
 *
 * @param[in] fDIArmingStateCallback 回调函数指针
 * 回调函数参数
 *		param[in] handle ZXVNMS_InitSession 返回的handle
 *		param[in] pTDIArmingState DI布、撤防状态结构指针
 *
 * @return 0 成功
 * @return <0失败
 */
extern "C" ZXVNMSSDK_EXPORT int __stdcall ZXVNMS_SetEncoderDIArmingStateCallback(
	void(__stdcall *fDIArmingStateCallback)(int handle,ZXVNMS_LPTEncoderDIArmingState pTDIArmingState,void* pUser),void* pUser);

/**
 * @brief 请求DI布、撤防状态上报，通过ZXVNMS_SetEncoderDIArmingStateCallback设置的回调函数上报
 *
 * @param[in] encoder_id 编码器id
 * @param[in] di_port DI通道号
 *
 * @return 0 成功
 * @return <0失败
 */
extern "C" ZXVNMSSDK_EXPORT int __stdcall ZXVNMS_RequestEncoderDIArmingState(const char* encoder_id,int di_port);

/**
 * @brief 查询所有编码器类型
 *
 * @return 0 成功
 * @return <0失败
 */
extern "C" ZXVNMSSDK_EXPORT int __stdcall ZXVNMS_QueryEncoderType();

/**
 * @brief 设置编码器GPS信息上报回调函数
 *
 * @param[in] fGPSInfoCallback 回调函数指针
 * 回调函数参数
 *		param[in] handle ZXVNMS_InitSession 返回的handle
 *		param[in] pGPSInfo GPS信息结构指针
 * @param[in] pUser 用户参数
 *
 * @return 0 成功
 * @return <0失败
 */
extern "C" ZXVNMSSDK_EXPORT int __stdcall ZXVNMS_SetGPSInfoCallback(
	void(__stdcall *fGPSInfoCallback)(int handle,ZXVNMS_LPTGPSInfo pGPSInfo,void* pUser),void* pUser);

/**
 * @brief GPS信息订阅
 *
 * @param[in] flag：1—订阅；0—取消订阅
 *
 * @return 0 成功
 * @return <0失败
 */
extern "C" ZXVNMSSDK_EXPORT int __stdcall ZXVNMS_GPSInfoSubscribe(int flag);

#endif

