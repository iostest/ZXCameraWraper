//////////////////////////////////////////////////////////////////////
// ZXVNMSClientSdk_DataType.h
//////////////////////////////////////////////////////////////////////
#if !defined(_ZXVNMS_CLIENT_SDK_DATATYPE_H_INCLUDED_)
#define _ZXVNMS_CLIENT_SDK_DATATYPE_H_INCLUDED_

//////////////////////////////////////////////////////////////////////
// 常量
//////////////////////////////////////////////////////////////////////

const int MAX_VIDEO_PLAY_COUNT = 20;	// 最大可同时实时浏览的路数
const int MAX_FILE_PLAY_COUNT  = 20;	// 最大可支持的文件回放路数
const int MAX_FILE_DOWNLOAD_COUNT = 20;	// 最大可支持的文件下载路数
const char* const EMPTY_STRING = "";	// 空字符串

const int MAX_LEN_21 = 21;				// 
const int MAX_LEN_51 = 51;				// 
const int MAX_LEN_16 = 16;				// 
const int MAX_LEN_255 = 255;			//
const int MAX_LEN_1024 = 1024;			//

// 查询结果类型
namespace ZXVNMS_DevType
{
	const int OFFICE = 0x00;			/**< 局站 */
	const int PLATFORM_DEVICE = 0x01;	/**< 平台设备 */
	const int ENCODER = 0x02;			/**< 编码器 */
	const int DECODER = 0x03;			/**< 解码器 */
	const int CAMERA = 0x04;			/**< 摄像头 */
	const int DI_DEVICE = 0x05;			/**< 告警设备DI */
	const int DO_DEVICE = 0x06;			/**< DO设备 */
	const int TV_WALL = 0x07;           /**< 电视墙 */

	const int PRESET  = 0x10;			/**< 预置位 */
	const int RECORDFILE = 0x11;		/**< 录像文件 */
	const int ALARMLINKAGE = 0x12;		/**< 告警联动 */
	const int ENCODERTYPE = 0x13;		/**< 编码器类型 */
	const int RTSPURL = 0x14;			/**< rtsp链接 */

	const int PLATE_DOMAIN = 0x20;		/**< 平台域 */
}

// 平台域的属性名
namespace ZXVNMS_Domain
{
	const char* const Domain = "domain";
	const char* const Id = "id";
	const char* const Name = "name";
	const char* const Ip = "ip";
	const char* const Port = "port";
	const char* const Link_mode = "link_mode";
	const char* const CmsType = "cmsType";
	const char* const Mduip = "mduip";
	const char* const Mduport = "mduport";
	const char* const State = "state";
	const char* const Domain_type = "domain_type";
	const char* const User_name = "user_name";
	const char* const User_password = "user_password";
	const char* const Domain_dll = "domain_dll";
}

// 局站的属性名
namespace ZXVNMS_Office
{
	const char* const Map = "Map";
	const char* const OfficeID = "OfficeID";
	const char* const OfficeName = "OfficeName";
	const char* const UpOfficeID = "UpOfficeID";
	const char* const city_id = "city_id";
	const char* const province_id = "province_id";
}

// 平台设备的属性名
namespace ZXVNMS_Platform
{
	const char* const device_id = "device_id";
	const char* const device_name = "device_name";
	const char* const device_type = "device_type";	// 1－中心服务器;2－接入服务器;4－转发服务器;8－存储服务器
	const char* const login_state = "login_state";
	const char* const office_id = "office_id";
	const char* const privilege_flag = "privilege_flag";
	const char* const service_addr = "service_addr";
	const char* const service_port = "service_port";
	const char* const web_page = "web_page";
}

// 编码器设备的属性名
namespace ZXVNMS_Encoder
{
	const char* const connect_server_id = "connect_server_id";
	const char* const device_id = "device_id";
	const char* const device_name = "device_name";
	const char* const device_type = "device_type";
	const char* const dispatch_server_id = "dispatch_server_id";
	const char* const encoder_state = "encoder_state";
	const char* const encoder_type = "encoder_type";
	const char* const ip_address = "ip_address";
	const char* const office_id = "office_id";
	const char* const privilege_flag = "privilege_flag";
	const char* const web_page = "web_page";
}

// 解码码器设备的属性名
namespace ZXVNMS_Decoder
{
	const char* const connect_server_id = "connect_server_id";
	const char* const device_id = "device_id";
	const char* const device_name = "device_name";
	const char* const device_type = "device_type";
	const char* const ip_address = "ip_address";
	const char* const office_id = "office_id";
	const char* const privilege_flag = "privilege_flag";
	const char* const web_page = "web_page";
}

// 摄像头设备的属性名
namespace ZXVNMS_Camera
{
	const char* const address = "address";
	const char* const category_id = "category_id";
	const char* const control_port = "control_port";
	const char* const device_id = "device_id";
	const char* const device_name = "device_name";
	const char* const device_type = "device_type";
	const char* const inport = "inport";
	const char* const is_controlable = "is_controlable";
	const char* const office_id = "office_id";
	const char* const parent_device_id = "parent_device_id";
	const char* const port_param = "port_param";
	const char* const pos_control = "pos_control";
	const char* const privilege_flag = "privilege_flag";
	const char* const ptz_protocol = "ptz_protocol";
}

// DI设备的属性名
namespace ZXVNMS_DI
{
	const char* const alarm_level = "alarm_level";
	const char* const alarm_type = "alarm_type";
	const char* const category_id = "category_id";
	const char* const description = "description";
	const char* const device_id = "device_id";
	const char* const device_name = "device_name";
	const char* const device_type = "device_type";
	const char* const di_port = "di_port";
	const char* const di_state = "di_state";
	const char* const encoder_deviceid = "encoder_deviceid";
	const char* const office_id = "office_id";
	const char* const privilege_flag = "privilege_flag";
}

// DO设备的属性名
namespace ZXVNMS_DO
{
	const char* const category_id = "category_id";
	const char* const device_id = "device_id";
	const char* const device_name = "device_name";
	const char* const device_type = "device_type";
	const char* const do_port = "do_port";
	const char* const do_state = "do_state";
	const char* const encoder_deviceid = "encoder_deviceid";
	const char* const office_id = "office_id";
	const char* const privilege_flag = "privilege_flag";
}

// 预置位的相关属性名
namespace ZXVNMS_Preset
{
	const char* const preset = "preset";
	const char* const description = "description";
}

// 告警联动的相关属性名
namespace ZXVNMS_AlarmLinkage
{
	const char* const alarm_device_id = "alarm_device_id";
	const char* const linkage_device_id = "linkage_device_id";
	const char* const alarm_type = "alarm_type";
	const char* const linkage_type = "linkage_type";
	const char* const trigger_state= "trigger_state";
	const char* const step = "step";
	const char* const param1 = "param1";
}

// 电视墙设备的属性名
namespace ZXVNMS_TVWALL
{
	const char* const device_id = "device_id";
	const char* const device_name = "device_name";
	const char* const device_type = "device_type";
	const char* const office_id = "office_id";
	const char* const privilege_flag = "privilege_flag";
}

// 录像文件的属性名
namespace ZXVNMS_RecordFile
{
	const char* const encoder_id = "encoder_id";
	const char* const service_addr = "service_addr";
	const char* const service_port = "service_port";
	const char* const outport = "outport";
	const char* const starttime = "starttime";
	const char* const endtime = "endtime";
	const char* const type = "type";
	const char* const filename = "filename";
	const char* const size = "size";
}

// 编码器类型的属性名
namespace ZXVNMS_EncoderType
{
	const char* const manufacture_id = "manufacture_id";
	const char* const manufacture_name = "manufacture_name";
	const char* const type_id = "typeid";
	const char* const type_name = "type_name";
}

// rtsp链接的属性名
namespace ZXVNMS_RtspUrl
{
	const char* const camera_id = "camera_id";					// 摄像头id
	const char* const device_name = "device_name";				// 摄像头名称
	const char* const connect_device = "connect_device";		// 编码器id
	const char* const connect_port = "connect_port";			// 通道号
	const char* const rtsp = "rtsp";							// rtsp链接
}

// 摄像头状态
namespace ZXVNMS_CameraState
{
	const int ONLINE = 0;				// 在线
	const int OFFLINE = 1;				// 断线
}

//////////////////////////////////////////////////////////////////////
// 结构
//////////////////////////////////////////////////////////////////////

#pragma pack(1)
// 局站
typedef struct
{
	int office_id;	// 局站id
	char office_name[MAX_LEN_51];	// 局站名称
	int upoffice_id;	// 上级局站id
	char province_id[MAX_LEN_16];	// 省id
	char city_id[MAX_LEN_16];	// 市id
	char map[MAX_LEN_51];	// 地图
}ZXVNMS_TOffice,*ZXVNMS_LPTOffice;


// 解码器
typedef struct
{
	int office_id;	// 局站id
	char device_id[MAX_LEN_21];	// 解码器id
	char device_name[MAX_LEN_51];	// 解码器名称
	char ip_address[MAX_LEN_21];	// ip地址
	char connect_server_id[MAX_LEN_21];	// 所属接入服务器id
}ZXVNMS_TDecoder,*ZXVNMS_LPTDecoder;

// 编码器
typedef struct
{
	int office_id;	// 局站id
	char device_id[MAX_LEN_21];	// 编码器id
	char device_name[MAX_LEN_51];	// 编码器名称
	char ip_address[MAX_LEN_21];	// ip地址(SDK PU必填)
	int listen_port;	// 监听端口(SDK PU必填)
	char device_user_name[MAX_LEN_51];	// 设备用户名(SDK PU必填)
	char pswd[MAX_LEN_21];	// 设备密码(SDK PU必填)
	int manufacture_id;	// 厂家id
	int device_type;	// 编码器类型
	char connect_server_id[MAX_LEN_21];	// 所属接入服务器id
	char dispatch_server_id[MAX_LEN_21];	// 所属转发服务器id
}ZXVNMS_TEncoder,*ZXVNMS_LPTEncoder;

// 摄像头
typedef struct
{
	int office_id;	// 局站id
	char device_id[MAX_LEN_21];	// 摄像头id
	char device_name[MAX_LEN_51];	// 摄像头名称
	char encoder_deviceid[MAX_LEN_21];	// 编码器id
	int inport;	// 通道号
	int manufacture_id;	// 厂家id
	int is_controlable;	// 是否可控
	int link_model;	// 视频连接方式 0－TCP;1－UDP;2－RTP
	char store_server_id[MAX_LEN_21];	// 存储服务器id，不存储填"0"
}ZXVNMS_TCamera,*ZXVNMS_LPTCamera;

// DI
typedef struct
{
	int office_id;	// 局站id
	char device_id[MAX_LEN_21];	// DI id
	char device_name[MAX_LEN_51];	// DI名称
	char encoder_deviceid[MAX_LEN_21];	// 编码器id
	int di_port;	// 通道号
}ZXVNMS_TDI,*ZXVNMS_LPTDI;

// DO
typedef struct
{
	int office_id;	// 局站id
	char device_id[MAX_LEN_21];	// DO id
	char device_name[MAX_LEN_51];	// DO名称
	char encoder_deviceid[MAX_LEN_21];	// 编码器id
	int do_port;	// 通道号
}ZXVNMS_TDO,*ZXVNMS_LPTDO;

// 告警
typedef struct
{
	char encoder_id[MAX_LEN_21];		// 编码器ID
	int channel;					    // 如果是DI告警表示DI口，如果是移动侦测告警和无视频告警则表示是视频端口
	int alarmtype;					    // 告警类型 1：DI  3：无视频告警  4：移动侦测
	char alarmtime[MAX_LEN_21];			// 告警时间，格式:yyyy-mm-dd hh:mm:ss
	int action;					        // 1：告警产生 0：告警消除
	char extendpara[MAX_LEN_1024];					// 告警扩展参数，由客户端解析，
										// 格式	para1name=para1value|para2name=para2value|...|
	int ishistory;						// 1：历史告警 0：实时告警
	int ishistoryend;					// 1：最后一条历史告警 0：不是最后一条
}ZXVNMS_TAlarm,*ZXVNMS_LPTAlarm;

// 异常
typedef struct
{
	char device_id[MAX_LEN_21];			// 设备类型
	int exceptiontype;					// 异常类型 100：设备断线
	char exceptiontime[MAX_LEN_21];		// 异常发生时间，格式:yyyy-mm-dd hh:mm:ss
	int action;					        // 1：异常产生 0：异常消除
	int ishistory;						// 1：历史异常 0：实时异常
	int ishistoryend;					// 1：最后一条历史异常 0：不是最后一条
}ZXVNMS_TException,*ZXVNMS_LPTException;

// 图像参数
typedef struct
{
	char encoder_id[MAX_LEN_21];		// 编码器ID
	int channel;				    	// 通道号
	int bright;					    	// 亮度
	int contrast;						// 对比度
	int gray;							// 灰度
	int saturation;						// 饱和度
}ZXVNMS_TPicParam,*ZXVNMS_LPTPicParam;

// 附加OSD
typedef struct
{
	char caption[MAX_LEN_51];			// OSD字幕
	int display;					    // 是否显示
	int osd_x;				        	// x坐标
	int osd_y;					        // y坐标
}ZXVNMS_TAdditionOSD,*ZXVNMS_LPTAdditionOSD;

// OSD
typedef struct
{
	char encoder_id[MAX_LEN_21];		// 编码器ID
	int channel;				    	// 通道号
	char channel_name[MAX_LEN_51];    	// 通道名称
	int osd_camera_disaplay;			// 是否显示通道名称 1 显示 0 不显示
	int osd_camera_x;				    // 通道名称显示x坐标
	int osd_camera_y;				    // 通道名称显示y坐标
	int osd_time_display;				// 是否显示时间 1 显示 0 不显示
	int osd_time_show_week;				// 是否显示星期	1 显示 0 不显示
	int osd_time_x;					    // 时间显示x坐标
	int osd_time_y;					    // 时间显示y坐标
	ZXVNMS_TAdditionOSD additionOSD[4];	// 附加OSD
}ZXVNMS_TOSD,*ZXVNMS_LPTOSD;

// 告警布防时间段
typedef struct
{
	int start_time;				    	// 开始时间，从00：00开始的分钟数
	int end_time;					    // 结束时间，从00：00开始的分钟数
}ZXVNMS_TAlarmDeployTime,*ZXVNMS_LPTAlarmDeployTime;

// 告警布防
typedef struct
{
	char encoder_id[MAX_LEN_21];		// 编码器ID
	int channel;					    // 如果是DI告警表示DI口，如果是移动侦测告警和无视频告警则表示是视频端口
	int alarmtype;				    	// 告警类型 1：DI  3：无视频告警  4:移动侦测
	int deploy_flag;			    	// 1 布防 0 撤防
	int is_clear;					    // 不在布防时间是否消除，1 消除 0 不消除
	ZXVNMS_TAlarmDeployTime deploytime[7][4];	// 从星期一到星期天，每天4个布防时间段
}ZXVNMS_TAlarmDeploy,*ZXVNMS_LPTAlarmDeploy;

// 码流设置
typedef struct  
{
	char encoder_id[MAX_LEN_21];        // 设备编码器id
	int  channel;                       // 通道号
    int  stream_kind;                   // 0: main_flow; 1: child_flow;
    int  stream_type;                   // 类型：视频流0、混合流1
    int  resolution;                    // 分辨率 0:DCIF 1:CIF 2:QCIF 3:4CIF 4:2CIF 5:QVGA
    int  bitrate_type;                  // 码率类型: 定码流，变码流
    int  pic_quality;                   // 图像质量 0:最好 1:次好 3:较好 4:一般 5:较差 6:差
    int  video_bitrate;                 // 码率上限 1:16  2:32  3:48  4:64  5:80  6:96  7:128  8:160  9:192  10:224
										// 11:256  12:320  13:384  14:448  15:512  16:640  17:768  18:896  19:1024
										// 20:1280  21:1536  22:1792  23:2048
    int  frame_rate;                    // 帧率 0:25  1:1  2:2  3:4  4:8  5:10  6:12  7:16  8:20
    char encodemode[MAX_LEN_21];          // H264; ...
}ZXVNMS_TEncodeStream,*ZXVNMS_LPTEncodeStream;

// PU本地录像及起始时间表结构
typedef struct  
{
    int  record_type;                   // 0: 定时录像; 1: 移动侦测; 2: 报警触发
    int  start_hour;                    // 开始时间，小时：0-24
    int  start_min;                     // 开始时间，分钟：0-59
    int  stop_hour;                     // 结束时间，小时：0-24
    int  stop_min;                      // 结束时间，分钟：0-59, 全天表示为：0, 0, 24, 0
}ZXVNMS_TSchedule,*ZXVNMS_LPTSchedule;

// PU本地录像设置参数结构
typedef struct
{
    char device_id[MAX_LEN_21];         // PU设备编号
    int  video_id;                      // 视频通道: 1, 2, 3, ...
    int  record_enable;                 // 0: 停止录像; 1: 开始录像
    int  recycle_rec_enable;            // 0: 否，录满后不覆盖; 1: 是，录满后覆盖，循环录像
    ZXVNMS_TSchedule schedules[7][4];   // 每周7天，每天4个录像时间段
}ZXVNMS_TPUStorageTask,*ZXVNMS_LPTPUStorageTask;

// 录像存储参数结构
typedef struct  
{
    char encoder_id[MAX_LEN_21];        // 编码器ID
    int  channel;                       // 外出端口
    int  store_model;                   // 存储模式: 0: 不存储; 1: 按时间段存储; 2: 告警触发存储

    char sunday[MAX_LEN_21];            // 参数指示时间段设置信息：每个字符为16进制字符串形式
    char monday[MAX_LEN_21];            // 对于每个字符，其表示的值中对应的每一位表示半个小时
    char tuesday[MAX_LEN_21];           // 若位值为1，则说明该半小时时间段为录像时间段
    char wednesday[MAX_LEN_21];         // 如："FF000000FFFF"表示为：
    char thursday[MAX_LEN_21];          //    当前的前4个小时和后8个小时为录像时间段
    char friday[MAX_LEN_21];            // 以下字符传信息相同
    char saturday[MAX_LEN_21];          // 
    char holiday[MAX_LEN_21];           //
	char store_server_id[MAX_LEN_21];   // 存储服务器ID

    int  pre_record;                    // 预录时间，单位(单位：秒)
    int  record_fg;                     // 1: 延时录制; 2: 告警录制
    int  delay_time;                    // 告警延时时间(单位：秒)
    int  saveday;                       // 存储天数
    int  is_space;                      // 按空间存储标识，0: 不按空间存储; 1: 按空间存储
    int  space_size;                    // 空间大小，单位M
}ZXVNMS_TVideoStorageConfig,*ZXVNMS_LPTVideoStorageConfig;

// 预置位参数结构
typedef struct
{
	char camera_id[MAX_LEN_21];			// 摄像头ID
	int preset;							// 预置位序号
	char description[MAX_LEN_51];		// 预置位描述
} ZXVNMS_TPreset, *ZXVNMS_LPTPreset;

// 告警联动参数结构
typedef struct
{
	char alarm_device_id[MAX_LEN_21];	// 告警设备ID
	char linkage_device_id[MAX_LEN_21];	// 联动设备ID	
	int alarm_type;						// 告警类型：1－DI告警，2－硬盘满，3－无视频告警，4－移动侦测，
										// 5－硬盘未格式化，6－读写硬盘出错，7－遮挡告警，8－制式不匹配，9－非法访问
	int linkage_type;					// 联动类型：1－转动摄像头，2－录像，3－DO设备状态变化
	int trigger_state;					// 触发条件：1-告警触发，2-告警消除触发
	int step;							// 联动步骤：1, 2, 3, ...
	int param1;							// 附加参数：联动类型1时表示预置位号
}ZXVNMS_TAlarmLinkage,*ZXVNMS_LPTAlarmLinkage;

// 编码器DI联动内容参数结构
typedef struct
{
	int port;							// 联动端口号，DO或视频通道号：1, 2, 3, ...
	int enable;							// 是否联动：1—联动，0—不联动
}ZXVNMS_TDILinkage,*ZXVNMS_LPTDILinkage;

// 编码器DI告警参数结构
typedef struct
{
	char encoder_id[MAX_LEN_21];		// 编码器ID
	int diport;							// DI通道号：1, 2, 3, ...
	int enable;							// 是否联动：1—联动，0—不联动
	int status;							// 告警状态：1—闭合，0—断开
	ZXVNMS_TDILinkage dolinkage[MAX_LEN_16];		// 联动DO
	ZXVNMS_TDILinkage recordlinkage[MAX_LEN_16];	// 联动录像
	ZXVNMS_TDILinkage shootlinkage[MAX_LEN_16];		// 联动抓图
}ZXVNMS_TEncoderDIAlarm,*ZXVNMS_LPTEncoderDIAlarm;

// 编码器DI、DO、视频通道号参数结构
typedef struct
{
	int dinum[MAX_LEN_16];				// DI通道号：1, 2, 3, ...
	int donum[MAX_LEN_16];				// DO通道号：1, 2, 3, ...
	int videonum[MAX_LEN_16];			// 视频通道号：1, 2, 3, ...
}ZXVNMS_TChannelNum,*ZXVNMS_LPTChannelNum;

// 解码器状态参数结构
typedef struct
{
	char decoder_id[MAX_LEN_16];		// 解码器ID
	int channel;						// 解码通道号：1, 2, 3, ...
	int dectype;						// 解码类型：0—实时视频，1—文件回放
	int decstate;						// 解码器状态：0—正常播放, 1—连接服务器失败, 2—视频终端
	int sound;							// 声音：1—开，0—关
	char filename[MAX_LEN_255];			// 播放文件名，解码类型为文件回放时有效
	char camera_id[MAX_LEN_16];			// 摄像头id
	int filesize;						// 文件大小（字节）
	int percent;						// 文件回放百分比1~100
	int speed;							// 播放速度
	int state;							// 文件回放状态：0—正常，1—暂停
}ZXVNMS_TDecodeState,*ZXVNMS_LPTDecodeState;

// DI布防状态参数结构
typedef struct
{
	char encoder_id[MAX_LEN_16];		// 编码器ID
	int diport;						// DI通道号：1, 2, 3, ...
	int armingstate;					// 布、撤防状态：0—撤防，1—布防
	int timestamp;						// 状态改变时间戳
}ZXVNMS_TEncoderDIArmingState,*ZXVNMS_LPTEncoderDIArmingState;

// GPS信息结构
typedef struct
{
	char encoder_id[MAX_LEN_16];			// 编码器ID
	char reporttime[MAX_LEN_21];			// 上报时间 YYYY-MM-DD hh:mm:ss(时分秒)格式
	char state[MAX_LEN_16];					// 定位状态 A=有效定位  V=无效定位
	char latitude[MAX_LEN_16];				// 纬度 ddmm.mmmm(度分)格式(前面的0也将被传输)
	char lathemisphere[MAX_LEN_16];			// 纬度半球 N(北半球) S(南半球)
	char longitude[MAX_LEN_16];				// 经度 dddmm.mmmm度分)格式(前面的0也将被传输)
	char longhemisphere[MAX_LEN_16];		// 经度半球 E(东经) W(西经)
	char groundspeed[MAX_LEN_16];			// 地面速率，单位：公里/小时
	char groundcourse[MAX_LEN_16];			// 地面航向 000.0~359.9度，以真北为参考基准，前面的0也将被传输
	char mode[MAX_LEN_16];					// 模式指示 A=自主定位，D=差分，E=估算，N=数据无效
}ZXVNMS_TGPSInfo,*ZXVNMS_LPTGPSInfo;
#pragma pack()

#endif