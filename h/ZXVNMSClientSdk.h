#if !defined(_ZXVNMS_CLIENT_SDK_H_INCLUDED_)
#define _ZXVNMS_CLIENT_SDK_H_INCLUDED_

#ifdef WIN32
	#ifdef ZXVNMSSDK_EXPORTS
		#define ZXVNMSSDK_EXPORT __declspec(dllexport)
	#else
		#define ZXVNMSSDK_EXPORT __declspec(dllimport)
	#endif
#else
	#define ZXVNMSSDK_EXPORT
#endif

#include "ZXVNMSClientSdk_DataType.h"

//////////////////////////////////////////////////////////////////////
// 接口函数
//////////////////////////////////////////////////////////////////////

/******************************初始化*********************************/

/**
 * @brief 初始化接口
 *
 * @return 0 成功
 * @return <0失败
 */
extern "C" ZXVNMSSDK_EXPORT int __stdcall ZXVNMS_Init();

/**
 * @brief 资源释放接口
 *
 * @return 0 成功
 * @return <0失败
 */
extern "C" ZXVNMSSDK_EXPORT int __stdcall ZXVNMS_Free();

/******************************用户注册*******************************/

/**
 * @brief 设置与平台网络事件回调函数
 *
 * @param[in] fNetEventCallback 回调函数指针
 * 回调函数参数
 *    param[in] dwType 事件类型： 1——连接；2——中断
 *    param[in] handle InitSession返回的hanlde,对应所连接的平台
 *    param[in] pUser 用户参数
 * @param[in] pUser 用户参数
 *
 * @return 0 成功
 * @return <0失败
 */
extern "C" ZXVNMSSDK_EXPORT int __stdcall ZXVNMS_SetNetEventMessageCallback(
	void(__stdcall *fNetEventCallback)(int dwType,int handle,void *pUser),void *pUser);

/**
 * @brief 指定登录域
 *
 * @param[in] fieldName 域名称
 *
 * @return 0 成功
 * @return <0失败
 */
extern "C" ZXVNMSSDK_EXPORT int __stdcall ZXVNMS_SetLoginField(const char* fieldName);

/**
 * @brief 登录指定平台
 *
 * @param[in] ip 中心管理服务器IP
 * @param[in] port 中心管理服务器端口
 * @param[in] userName 用户名
 * @param[in] pswd 用户密码
 * @param[in] ValidateType 认证类型：0－不需要附加认证；1－需要认证Mac；2－需要认证USB Key；3－需要条件1、2同时满足
 * @param[in] UserMacAddr MAC地址，没有默认为""
 * @param[in] UserUsbKey USBkey，没有默认为""
 * @param[in] Bound 是否绑定: 0——不绑定；1——绑定
 *
 * @return >=0 成功
 * @return <0失败
 */
extern "C" ZXVNMSSDK_EXPORT int __stdcall ZXVNMS_InitSession(
	const char* ip,
	int port,
	const char* userName,
	const char* pswd,
	int ValidateType,
	const char* UserMacAddr,
	const char* UserUsbKey,
	int Bound);

/**
 * @brief 获取用户ID
 * 
 * @return 未登录时返回""
 */
extern "C" ZXVNMSSDK_EXPORT const char* __stdcall ZXVNMS_GetUserID();

/**
 * @brief 获取用户类型
 * 
 * @return >0 成功，详见具体用户类型
 * @return <0 失败
 */
extern "C" ZXVNMSSDK_EXPORT int __stdcall ZXVNMS_GetUserType();

/**
 * @brief 获取会话ID
 * 
 * @return 未登录时返回""
 */
extern "C" ZXVNMSSDK_EXPORT const char* __stdcall ZXVNMS_GetSessionID();

/**
 * @brief 获取中心服务器IP
 * 
 * @return 未登录时返回""
 */
extern "C" ZXVNMSSDK_EXPORT const char* __stdcall ZXVNMS_GetCMSIP();

/**
 * @brief 获取中心服务器端口
 * 
 * @return 未登录时返回0
 */
extern "C" ZXVNMSSDK_EXPORT int __stdcall ZXVNMS_GetCMSPort();

/**
 * @brief 注销用户
 *
 * @param[in] handle 用户登录成功返回的句柄
 *
 * @return 0 成功
 * @return <0失败
 */
extern "C" ZXVNMSSDK_EXPORT int __stdcall ZXVNMS_TerminateSession(int handle);

/**
 * @brief 切换平台
 *
 * @param[in] handle 用户登录成功返回的句柄
 *
 * @return 0 成功
 * @return <0失败
 */
extern "C" ZXVNMSSDK_EXPORT int __stdcall ZXVNMS_SwitchCMS(int handle);

/******************************设备获取******************************/

/**
 * @brief 查询设备列表
 *
 * @param[in] devType
	OFFICE = 0x00		   局站        
 	PLATFORM_DEVICE = 0x01 平台设备
	ENCODER  = 0x02        编码器 
	DECODER  = 0x03        解码器
	CAMERA   = 0x04        摄像头
	ALARM_DEVICE = 0x05    告警输入设备DI
	DO_DEVICE = 0x06       告警输出设备
 *
 * @return 0 成功
 * @return <0失败
 */
extern "C" ZXVNMSSDK_EXPORT int __stdcall ZXVNMS_QueryDevices(int devType);

/**
 * @brief 查询设备列表
 *
 * @param[in] devType
	OFFICE = 0x00		   局站        
 	PLATFORM_DEVICE = 0x01 平台设备
	ENCODER  = 0x02        编码器 
	DECODER  = 0x03        解码器
	CAMERA   = 0x04        摄像头
	ALARM_DEVICE = 0x05    告警输入设备DI
	DO_DEVICE = 0x06       告警输出设备
	@param[in] domain:域标识,如果是查询本域设备列表则为空,否则为对应域的标识全名
 *
 * @return 0 成功
 * @return <0失败
 */
extern "C" ZXVNMSSDK_EXPORT int __stdcall ZXVNMS_QueryDevicesForDomain(int devType,const char* domain);

/**
 * @brief 查询预置位
 *
 * @param[in] cameraID
 *
 * @return 0 成功
 * @return <0失败
 */
extern "C" ZXVNMSSDK_EXPORT int __stdcall ZXVNMS_QueryPreset(const char* cameraID);

/**
* @brief 查询告警联动
*
* @param[in] alarm_device_id
*
* @return 0 成功
* @return <0失败
*/
extern "C" ZXVNMSSDK_EXPORT int __stdcall ZXVNMS_QueryAlarmLinkage(const char* alarm_device_id);

/**
 * @brief 数据访问游标后移(数据遍历)
 *
 * @return 0  成功
 * @return -1 遍历完成，到达结果集末尾
 * @return <0 失败
 */
extern "C" ZXVNMSSDK_EXPORT int __stdcall ZXVNMS_MoveNext();

/**
 * @brief 数据访问游标移动到第一个元素位置
 *
 * @return 0  成功
 * @return <0 失败
 */
extern "C" ZXVNMSSDK_EXPORT int __stdcall ZXVNMS_MoveFirst();

/**
 * @brief 设置当前所要遍历的结果集(设备或者查询结果)类型
 *        在需要重新遍历某个已查询的结果集时，调用该接口
 *
 * @param devType 参考ZXVNMS_DevType中所定义的
 *
 * @return 0  成功
 * @return <0 失败
 */
extern "C" ZXVNMSSDK_EXPORT int __stdcall ZXVNMS_SetCurrentDevType(int devType);

/**
 * @brief 获取设备字符串属性
 *
 * @param[in] propertyName 属性名
 *
 * @return 获取失败返回""
 */
extern "C" ZXVNMSSDK_EXPORT const char* __stdcall ZXVNMS_GetValueStr(const char* propertyName);

/**
 * @brief 获取设备整形属性
 *
 * @param[in] propertyName 属性名
 *
 * @return 获取失败返回INT_MIN
 */
extern "C" ZXVNMSSDK_EXPORT int __stdcall ZXVNMS_GetValueInt(const char* propertyName);

/**
 * @brief 播放视频
 *
 * @param[in] cameraID 摄像头ID
 * @param[in] hWnd 播放窗口句柄
 *
 * @return >=0 实时视频播放句柄
 * @return <0失败
 */
extern "C" ZXVNMSSDK_EXPORT int __stdcall ZXVNMS_PlayVideo(const char* cameraID,long hWnd);

/**
 * @brief 停止视频
 *
 * @param[in] handle 实时视频播放句柄
 *
 * @return 0 成功
 * @return <0失败
 */
extern "C" ZXVNMSSDK_EXPORT int __stdcall ZXVNMS_StopVideo(int handle);

/**
 * @brief 云镜控制
 *
 * @param[in] cameraID 摄像头ID
 * @param[in] dwPTZCommand 控制命令 
	 8:  设置预置位
	 9:  删除预置位
	 11: 镜头拉近
	 12: 镜头移远
	 13: 焦点前调
	 14: 焦点后调
	 15: 光圈扩大
	 16: 光圈缩小
	 21: 镜头向上
	 22: 镜头向下
	 23: 镜头向左
	 24: 镜头向右
	 39: 到达预置位
 * @param[in] param1 1 开始动作 0 停止动作,当dwPTZCommand为8,9,39时,param1为预置位序号,最多255,具体和球机有关
 * @param[in] param2 云台控制速度
 * @param[in] param3 保留
 * @param[in] param4 保留
 *
 * @return 0 成功
 * @return <0失败
 */
extern "C" ZXVNMSSDK_EXPORT int __stdcall ZXVNMS_ControlCamera(
	const char* cameraID,
	int dwPTZCommand,
	int param1,
	int param2,
	int param3,
	int param4);

/**
 * @brief 云镜控制(自动争控)
 *	具有争控功能，且不支持万能控制协议的平台，用此接口控制云台
 *  调用此接口时，如没有控制权限，会自动请求控制权限
 *
 * @param[in] cameraID 摄像头ID
 * @param[in] dwPTZCommand 控制命令 
	 8:  设置预置位
	 9:  删除预置位
	 11: 镜头拉近
	 12: 镜头移远
	 13: 焦点前调
	 14: 焦点后调
	 15: 光圈扩大
	 16: 光圈缩小
	 21: 镜头向上
	 22: 镜头向下
	 23: 镜头向左
	 24: 镜头向右
	 39: 到达预置位
 * @param[in] param1 1 开始动作 0 停止动作,当dwPTZCommand为8,9,39时,param1为预置位序号,最多255,具体和球机有关
 * @param[in] param2 云台控制速度
 * @param[in] param3 保留
 * @param[in] param4 保留
 *
 * @return 0 成功
 * @return <0失败
 */
extern "C" ZXVNMSSDK_EXPORT int __stdcall ZXVNMS_ControlCameraAutoReq(
	const char* cameraID,
	int dwPTZCommand,
	int param1,
	int param2,
	int param3,
	int param4);

/**
 * @brief 云镜控制
 *
 * @param[in] pszPUID 编码器ID
 * @param[in] nChannel 通道号
 * @param[in] dwPTZCommand 控制命令 
	 8:  设置预置位
	 9:  删除预置位
	 11: 镜头拉近
	 12: 镜头移远
	 13: 焦点前调
	 14: 焦点后调
	 15: 光圈扩大
	 16: 光圈缩小
	 21: 镜头向上
	 22: 镜头向下
	 23: 镜头向左
	 24: 镜头向右
	 39: 到达预置位
 * @param[in] param1 1 开始动作 0 停止动作,当dwPTZCommand为8,9,39时,param1为预置位序号,最多255,具体和球机有关
 * @param[in] param2 云台控制速度
 * @param[in] param3 保留
 * @param[in] param4 保留
 *
 * @return 0 成功
 * @return <0失败
 */
extern "C" ZXVNMSSDK_EXPORT int __stdcall ZXVNMS_ControlCameraEx(
	const char* pszPUID,
	int nChannel,
	int dwPTZCommand,
	int param1,
	int param2,
	int param3,
	int param4);

/**
 * @brief 打开/关闭声音
 *
 * @param[in] cameraID 摄像头ID
 * @param[in] bOpen true：打开 false：关闭
 *
 * @return 0 成功
 * @return <0失败
 */
extern "C" ZXVNMSSDK_EXPORT int __stdcall ZXVNMS_OpenSound(
	const char* cameraID,
	bool bOpen);

/**
 * @brief ZXVNMS_QueryRecordFile 查询录像,通过编码器ID和通道
 *
 * @param[in] pszPUID		PUID
 * @param[in] nChannel		PU端口
 * @param[in] startTime		开始时间  //YYYY-MM-DD HH:mm:ss
 * @param[in] endTime		结束时间  //YYYY-MM-DD HH:mm:ss
 * @param[in] nType			类型--0为所有
 * @param[in] locate		是否前端1-是，0存储服务器
 *
 * @return 0 成功
 * @return <0失败
*/
extern "C" ZXVNMSSDK_EXPORT int __stdcall ZXVNMS_QueryRecordFile(
	const char* pszPUID,
	int nChannel, 
	const char* startTime, 
	const char* endTime, 
	int nType, 
	int locate);
/**
 * @brief ZXVNMS_QueryRecordFileEx查询录像,通过摄像头ID
 *
 * @param[in] cameraID		摄像头ID
 * @param[in] startTime		开始时间  //YYYY-MM-DD HH:mm:ss
 * @param[in] endTime		结束时间  //YYYY-MM-DD HH:mm:ss
 * @param[in] nType			类型--0为所有
 * @param[in] isForword		是否前端1-是，0存储服务器
 *
 * @return 0 成功
 * @return <0失败
*/
extern "C" ZXVNMSSDK_EXPORT int __stdcall ZXVNMS_QueryRecordFileEx(
	const char* cameraID,
	const char* startTime,
	const char* endTime,
	int nType,
	int locate);
/*
 * @brief ZXVNMS_PlayFile 文件回放
 * 
 * @param[in] cameraID  摄像头ID
 * @param[in] filename  文件名
 * @param[in] filesize	文件大小（字节）
 * @param[in] locate    文件位置
 * @param[in] hWnd      回放窗口句柄
 *
 * @return >=0 文件回放句柄
 * @return <0 失败
 */
extern "C" ZXVNMSSDK_EXPORT int __stdcall ZXVNMS_PlayFile(
	const char* cameraID,
	const char* filename,
	int filesize,
	int locate,
	long hWnd);
/*
 *@brief ZXVNMS_StopFilePlay 停止文件回放
 * 
 * @param[in] handle ZXVNMS_PlayFile返回的播放句柄
 *
 * @return 0 成功
 * @return <0 失败
 */
extern "C" ZXVNMSSDK_EXPORT int __stdcall ZXVNMS_StopFilePlay(int handle);

/*
 *@brief ZXVNMS_SetFileOffset 设置文件回放偏移量
 * 
 * @param[in] handle ZXVNMS_PlayFile返回的播放句柄
 * @param[in] offset 文件偏移量百分比 0 ~ 100
 *
 * @return 0 成功
 * @return <0 失败
 */
extern "C" ZXVNMSSDK_EXPORT int __stdcall ZXVNMS_SetFilePlayOffset(int handle,int offset);

/**
 * @brief PauseFilePlay 暂停播放
 *
 * @param[in] handle PlayFile返回的播放句柄
 * @param[in] flag true:暂停 false:取消暂停
 *
 * @return 0 成功 <0失败
 */
extern "C" ZXVNMSSDK_EXPORT int __stdcall ZXVNMS_PauseFilePlay(int handle,bool bPause);

/*
 *  @brief SlowFilePlay 
 *
 *  @param[in] handle PlayFile返回的播放句柄
 *
 *  @return 0 成功，<0失败
 */
extern "C" ZXVNMSSDK_EXPORT int __stdcall ZXVNMS_SlowFilePlay(int handle);

/*
 *	@brief FastFilePlay
 *
 *  @param[in] handle PlayFile返回的播放句柄
 *
 *  @return 0 成功 <0失败
 */
extern "C" ZXVNMSSDK_EXPORT int __stdcall ZXVNMS_FastFilePlay(int handle);

/**
 * @brief 获取视频码率（实时视频、录像回放、录像下载）
 *
 * @param[in] handle 实时视频、录像回放、录像下载句柄
 * @param[out] pVal 视频码率指针(码率单位:Byte/s)
 *
 * @return 0	成功
 * @return <0	失败
 */
extern "C" ZXVNMSSDK_EXPORT int __stdcall ZXVNMS_GetVideoDataRate(int handle,long* pVal);

/**
 * @brief 获取视频码率（实时视频、录像回放、录像下载）
 *
 * @param[in] sessionhandle ZXVNMS_InitSession返回的handle
 * @param[in] handle 实时视频、录像回放、录像下载句柄
 * @param[out] pVal 视频码率指针(码率单位:Byte/s)
 *
 * @return 0	成功
 * @return <0	失败
 */
extern "C" ZXVNMSSDK_EXPORT int __stdcall ZXVNMS_GetVideoDataRateEx(int sessionhandle,int handle,long* pVal);

/**
 * @brief 获取文件回放、文件下载百分比
 *
 * @param[in] handle 文件回放、文件下载句柄
 * @param[out] pVal 文件回放、文件下载百分比指针
 *
 * @return 0	成功
 * @return <0	失败
 */
extern "C" ZXVNMSSDK_EXPORT int __stdcall ZXVNMS_GetFilePercent(int handle,long* pVal);

/**
 * @brief 获取文件回放、文件下载百分比
 *
 * @param[in] sessionhandle ZXVNMS_InitSession返回的handle
 * @param[in] handle 文件回放、文件下载句柄
 * @param[out] pVal 文件回放、文件下载百分比指针
 *
 * @return 0	成功
 * @return <0	失败
 */
extern "C" ZXVNMSSDK_EXPORT int __stdcall ZXVNMS_GetFilePercentEx(int sessionhandle,int handle,long* pVal);

/**
 * @brief 获取文件下载剩余时间
 *
 * @param[in] handle 文件下载句柄
 * @param[out] pVal 文件下载剩余时间(s)指针
 *
 * @return 0	成功
 * @return <0	失败
 */
extern "C" ZXVNMSSDK_EXPORT int __stdcall ZXVNMS_GetDownloadRemainTime(int handle,long* pVal);

/**
 * @brief 获取文件下载剩余时间
 *
 * @param[in] sessionhandle ZXVNMS_InitSession返回的handle
 * @param[in] handle 文件下载句柄
 * @param[out] pVal 文件下载剩余时间(s)指针
 *
 * @return 0	成功
 * @return <0	失败
 */
extern "C" ZXVNMSSDK_EXPORT int __stdcall ZXVNMS_GetDownloadRemainTimeEx(int sessionhandle,int handle,long* pVal);

/**
 * @brief 获取播放器状态
 *
 * @param[in] handle 实时视频、文件回放句柄
 * @param[out] pVal 播放器状态指针
 *
	*pVal = 1;	//准备就绪
	*pVal = 2;	//正在连接服务器.......
	*pVal = 3;	//服务器连接成功
	*pVal = 4;	//连接服务器失败
	*pVal = 5;	//正在获取视频路由.....
	*pVal = 6;	//正在尝试重新连接服务器
	*pVal = 7;	//正在请求视频 ....
	*pVal = 8;	//申请视频失败,不过这个状态预留
	*pVal = 9; 	//正在播放
	*pVal = 10; //正在播放并且正在录像
	*pVal = 11; //视频中断
	*pVal = 12; //视频中断并且正在录像
	*pVal = 13;	//已停止
	*pVal = 14;	//获取视频路由超时
	*pVal = 15;	//未获取到可用的视频路由
	*pVal = 16;	//请求视频超时
	*pVal = 17;	//连接服务器超时
	*pVal = 18;	//用户已被停用
	*pVal = 19; //连接错误
	*pVal = 20; //PU不在线
	*pVal = 21; //暂停
	*pVal = 22; //PU COM控件没有注册
	*pVal = 23; //媒体转发服务器不在线
	*pVal = 24; //存储服务器不在线
	*pVal = 0;  //其他
 *
 *
 * @return 0	成功
 * @return <0	失败
 */
extern "C" ZXVNMSSDK_EXPORT int __stdcall ZXVNMS_GetPlayerState(int handle,long* pVal);

/**
 * @brief 文件下载
 *
 * @param[in] cameraID		摄像头ID
 * @param[in] filename		文件名
 * @param[in] filesize		文件大小（字节）
 * @param[in] locate		文件位置 0:中心存储 1:前端存储
 * @param[in] savefilepath	本地文件保存全路径
 *
 * @return >=0 文件下载句柄
 * @return <0失败
 */
extern "C" ZXVNMSSDK_EXPORT int __stdcall ZXVNMS_DownloadFile(
	const char* cameraID,
	const char* filename,
	int filesize,
	int locate,
	const char* savefilepath);

/**
 * @brief 停止文件下载
 *
 * @param[in] handle ZXVNMS_DownloadFile返回的句柄
 *
 * @return 0 成功
 * @return <0失败
 */
extern "C" ZXVNMSSDK_EXPORT int __stdcall ZXVNMS_StopFileDownload(int handle);

/**
 * @brief 视频抓图
 *
 * @param[in] handle 实时视频、文件回放句柄
 * @param[in] savefilepath 图片保存路径
 *
 * @return 0	成功
 * @return <0	失败
 */
extern "C" ZXVNMSSDK_EXPORT int __stdcall ZXVNMS_CapPic(int handle,const char* savefilepath);

/**
 * @brief 开始本地录像
 *
 * @param[in] handle 实时视频、文件回放句柄
 * @param[in] savefilepath 录像保存路径
 *
 * @return 0	成功
 * @return <0	失败
 */
extern "C" ZXVNMSSDK_EXPORT int __stdcall ZXVNMS_StartRecordFile(
	int handle,
	const char* savefilepath);

/**
 * @brief 停止本地录像
 *
 * @param[in] handle 实时视频、文件回放句柄
 *
 * @return 0 成功
 * @return <0失败
 */
extern "C" ZXVNMSSDK_EXPORT int __stdcall ZXVNMS_StopRecordFile(int handle);

/**
 * @brief 查询PU类型
 *
 * @param[in] PUID 编码器ID
 *
 * @return -1 失败
 * @return !=-1 PU类型码
 */
extern "C" ZXVNMSSDK_EXPORT int __stdcall ZXVNMS_GetPUType(const char *PUID);

/**
 * @brief 设置双向语音回调函数
 *
 * @param[in] Notify	回调函数指针
  * 回调函数参数
  *		1 PU ID
  *		2 当前状态 1：正在发起请求 2：通话状态 3：中断 -1：请求错误
  *		3 如果当前状态为-1,该参数为具体的错误号，否则无意义
  *		4 user data
 * @param[in] userData	用户数据,回调时传回给调用者
 *
 * @return 0 成功
 */
extern "C" ZXVNMSSDK_EXPORT int __stdcall ZXVNMS_DuplexSetEventCallBack(
	void (__stdcall *Notify)(const char*, int, int,void*),void *userData);

/**
 * @brief 设置双向语音录音
 *
 * @param[in] flag 0 不录音 1 录音
 *
 * @return 0 成功
 * @return <0失败
 */
extern "C" ZXVNMSSDK_EXPORT int __stdcall ZXVNMS_DuplexSetStoreFlag(int flag);

/**
 * @brief 设置双向录音存储路径
 *
 * @param[in] filepath 存储路径
 *
 * @return 0 成功
 * @return <0失败
 */
extern "C" ZXVNMSSDK_EXPORT int __stdcall ZXVNMS_DuplexSetStoreFilePath(const char* filepath);

/**
 * @brief 开始双向语音
 *
 * @param[in] pszPUID 编码器ID
 *
 * @return 0 成功
 * @return <0失败
 */
extern "C" ZXVNMSSDK_EXPORT int __stdcall ZXVNMS_DuplexStartVoiceCom(const char* pszPUID);

/**
 * @brief 停止双向语音
 *
 * @param[in] pszPUID 编码器ID
 *
 * @return 0 成功
 * @return <0失败
 */
extern "C" ZXVNMSSDK_EXPORT int __stdcall ZXVNMS_DuplexStopVoiceCom(const char* pszPUID);

/**
 * @brief 开始语音广播
 *
 * @param[in] pszPUID 编码器ID
 *
 * @return 0 成功
 * @return <0失败
 */
extern "C" ZXVNMSSDK_EXPORT int __stdcall ZXVNMS_DuplexStartBroadCast(const char* pszPUID);

/**
 * @brief 停止语音广播
 *
 * @return 0 成功
 * @return <0失败
 */
extern "C" ZXVNMSSDK_EXPORT int __stdcall ZXVNMS_DuplexStopBroadCast();

/**
 * @brief 获得发送数据字节数
 *
 * @param[out] pVal 发送数据字节数
 *
 * @return 0 成功
 * @return <0失败
 */
extern "C" ZXVNMSSDK_EXPORT int __stdcall ZXVNMS_GetDuplexSendData(long* pVal);

/**
 * @brief 获得发送数据字节数
 *
 * @param[in] sessionhandle ZXVNMS_InitSession返回的handle
 * @param[out] pVal 发送数据字节数
 *
 * @return 0 成功
 * @return <0失败
 */
extern "C" ZXVNMSSDK_EXPORT int __stdcall ZXVNMS_GetDuplexSendDataEx(int sessionhandle,long* pVal);

/**
 * @brief 获得接收数据字节数
 *
 * @param[out] pVal 接收数据字节数
 *
 * @return 0 成功
 * @return <0失败
 */
extern "C" ZXVNMSSDK_EXPORT int __stdcall ZXVNMS_GetDuplexReceiveData(long* pVal);

/**
 * @brief 获得接收数据字节数
 *
 * @param[in] sessionhandle ZXVNMS_InitSession返回的handle
 * @param[out] pVal 接收数据字节数
 *
 * @return 0 成功
 * @return <0失败
 */
extern "C" ZXVNMSSDK_EXPORT int __stdcall ZXVNMS_GetDuplexReceiveDataEx(int sessionhandle,long* pVal);

/**
 * @brief 电视墙初始化
 *
 * @param[in] devID 电视墙ID
 *
 * @return 0 成功
 * @return <0失败
 */
extern "C" ZXVNMSSDK_EXPORT int __stdcall ZXVNMS_TVW_Init(const char* devID);

/**
 * @brief 实时视频上电视墙
 *
 * @param[in] devID		电视墙ID
 * @param[in] cameraId	摄像头ID
 * @param[in] wndIndex	电视墙编号
 *
 * @return 0 成功
 * @return <0失败
 */
extern "C" ZXVNMSSDK_EXPORT int __stdcall ZXVNMS_TVW_RealPlay(
	const char* devID,
	const char* cameraId,
	int wndIndex);

/**
 * @brief 获取电视墙状态
 *
 * @param[in] devID		电视墙ID
 * @param[in] wndIndex	电视墙窗口编号
 * @param[out] pTDecodeState	解码状态结构指针
 *
 * @return 通道数 成功
 * @return -1 失败
 */
extern "C" ZXVNMSSDK_EXPORT int __stdcall ZXVNMS_TVW_GetState(
	const char* devID,
	int wndIndex,
	ZXVNMS_LPTDecodeState pTDecodeState);

/**
 * @brief 电视墙控制
 *
 * @param[in] devID		电视墙ID
 * @param[in] wndIndex	电视墙编号
 * @param[in] cmd		PLAY/PAUSE/RESTART/SEEK/STOP/SPEED
 * @param[in] param		当cmd为SEEK时，参数为设置进度进度:0-100
 *						当cmd为SPEED时，参数为0,1,2,  0:快进 1:正常 2:慢进,支持1/2,1,2,4,8倍速播放
 *
 * @return 0 成功
 * @return <0失败
 */
extern "C" ZXVNMSSDK_EXPORT int __stdcall ZXVNMS_TVW_Control(
	const char* devID,
	int wndIndex,
	const char* cmd,
	int param);

/**
 * @brief 存储服务器文件上墙
 *
 * @param[in] devID		电视墙ID
 * @param[in] cameraId	摄像头ID
 * @param[in] fileName	录像文件名
 * @param[in] wndIndex	电视墙画面序号
 *
 * @return >=0 成功
 * @return <0失败
 */
extern "C" ZXVNMSSDK_EXPORT int __stdcall ZXVNMS_TVW_PlayServerFile(
	const char* devID,
	const char* cameraId,
	const char* fileName,
	int wndIndex);

/**
 * @brief 前端PU上的录像文件上墙
 *
 * @param[in] devID		电视墙ID
 * @param[in] cameraId	摄像头ID
 * @param[in] fileName	录像文件名
 * @param[in] wndIndex	电视墙画面序号
 *
 * @return >=0 成功
 * @return <0失败
 */
extern "C" ZXVNMSSDK_EXPORT int __stdcall ZXVNMS_TVW_PlayPUFile(
	const char* devID,
	const char* cameraId,
	const char* fileName,
	int wndIndex);

/**
 * @brief 设置视频流数据(包括实时流和文件流)回调函数
 *
 * @param[in] fStreamCallback 回调函数指针
 * 回调函数参数
 *    @param[in] handle ZXVNMS_StartVideoStream返回的handle
 *    @param[in] dataType 数据类型
 *    @param[in] data 接收数据缓冲区地址
 *    @param[in] size 接收数据字节数
 *    @param[in] pUser 用户参数
 * @param[in] pUser 用户参数
 *
 * @return 0 成功
 * @return <0失败
 */
extern "C" ZXVNMSSDK_EXPORT int __stdcall ZXVNMS_SetVideoStreamCallback(
	void(__stdcall *fStreamCallback)(int handle,int dataType,const char* data,int size,void *pUser),
	void *pUser);

/**
 * @brief 开始视频流数据
 *
 * @param[in] cameraID 摄像头ID
 * @return >=0 成功
 * @return <0失败
 */
extern "C" ZXVNMSSDK_EXPORT int __stdcall ZXVNMS_StartVideoStream(const char* cameraID);

/**
 * @brief 停止视频流数据
 *
 * @param[in] handle ZXVNMS_StartVideoStream返回的handle
 *
 * @return 0 成功
 * @return <0失败
 */
extern "C" ZXVNMSSDK_EXPORT int __stdcall ZXVNMS_StopVideoStream(int handle);

/*
 * @brief 开始视频文件流数据
 * 
 * @param[in] cameraID  摄像头ID
 * @param[in] filename  文件名
 * @param[in] filesize	文件大小（字节）
 * @param[in] locate    文件位置 0:中心存储 1:前端存储
 *
 * @return >=0 文件回放句柄
 * @return <0 失败
 */
extern "C" ZXVNMSSDK_EXPORT int __stdcall ZXVNMS_StartVideoFileStream(
	const char* cameraID,
	const char* filename,
	int filesize,
	int locate);

/**
 * @brief 停止视频文件流数据
 *
 * @param[in] handle ZXVNMS_StartVideoFileStream返回的handle
 *
 * @return 0 成功
 * @return <0失败
 */
extern "C" ZXVNMSSDK_EXPORT int __stdcall ZXVNMS_StopVideoFileStream(int handle);

/**
 * @brief 通过错误号获取错误信息
 *
 * @param[in] errcode	错误号
 * @param[out] pBuf		错误信息输出缓冲区
 * @param[out] bufSize	错误信息长度
 *
 * @return 0 成功
 * @return <0失败 
 */
extern "C" ZXVNMSSDK_EXPORT int __stdcall ZXVNMS_GetErrorInfo(int errcode,char* pBuf,int* bufSize);

/**
 * @brief 通过错误号获取错误信息
 *
 * @param[in] handle	视频句柄
 * @param[out] pBuf		上层开的缓冲，用来保存YUV数据
 * @param[in] nBufSize	缓冲大小
 * @param[out] pWidth	图片宽度指针，函数调用成功后将YUV图片的宽度通过该指针上传给上层
 * @param[out] pHeight	图片高度指针，函数调用成功后将YUV图片的高度通过该指针上传给上层
 *
 * @return 0 成功
 * @return <0失败 
 */
extern "C" ZXVNMSSDK_EXPORT int __stdcall ZXVNMS_SnapPicYUV(int handle,char* pBuf,int nBufSize,int* pWidth,int* pHeight);

/**
 * @brief 局部放大
 *
 * @param[in] handle	视频播放句柄
 * @param[in] x1		选中区域左上角x坐标（相对播放窗口）
 * @param[in] y1		选中区域左上角y坐标（相对播放窗口）
 * @param[in] x2		选中区域右上角x坐标（相对播放窗口）
 * @param[in] y2		选中区域右上角y坐标（相对播放窗口）
 *
 * @return 0 成功
 * @return <0失败 
 */
extern "C" ZXVNMSSDK_EXPORT int __stdcall ZXVNMS_RegionMainify(int handle,int x1,int y1,int x2,int y2);

/**
* @brief 查询设备是否存在
*
* @param[in] devType	设备类型
* @param[in] devID		设备编号
*
* @return 0 设备存在
* @return <0 设备不存在或执行失败 
*/
extern "C" ZXVNMSSDK_EXPORT int __stdcall ZXVNMS_CheckDeviceExist(int devType, const char* devID);

/**
* @brief 编码器对时
*
* @param[in] encoderID	编码器ID
*
* @return 0 成功
* @return <0 失败
*/
extern "C" ZXVNMSSDK_EXPORT int __stdcall ZXVNMS_SetEncoderTime(const char* encoderID);

/**
* @brief 设置解码器配置
*
* @param[in] decoderID	解码器ID
*
* @param[in] decoderChannel 解码通道
*
* @param[in] encoderID	编码器ID
*
* @param[in] encoderChannel	编码通道
*
* @return 0 成功
* @return <0 失败
*/
extern "C" ZXVNMSSDK_EXPORT int __stdcall ZXVNMS_SetDecodeConfig(const char* decoderID,int decoderChannel,const char* encoderID,int encoderChannel);

/**
* @brief 获取服务器运行状态
*
* @param[in] serverID	服务器ID
* @param[in] serverType	服务器类型
	0x0001	中心服务器
	0x0002	接入服务器
	0x0004	视频转发服务器
	0x0008	存储服务器
	0x1004	物理中心服务器
* @param[out] cpu		cpu占用率(%)
* @param[out] mem		内存占用率(%) 
* @param[out] netsend	网络收(Bytes/s) 
* @param[out] netrecv	网络发(Bytes/s) 	
*
* @return 0 成功
* @return <0 失败
*/
extern "C" ZXVNMSSDK_EXPORT int __stdcall ZXVNMS_GetServerRunState(
	const char* serverID,
	int serverType,
	int* cpu,
	int* mem,
	int* netsend,
	int* netrecv);

/**
 * @brief 设置YUV数据回调函数
 *
 * @param[in] fYUVCallback 回调函数指针
 * 回调函数参数
 *    @param[in] handle ZXVNMS_PlayVideo或ZXVNMS_PlayFile返回的handle
 *    @param[in] data	接收数据缓冲区地址
 *    @param[in] size	接收数据字节数
 *    @param[in] width	图片宽度
 *    @param[in] height	图片高度
 *    @param[in] pUser	用户参数
 * @param[in] pUser	用户参数
 *
 * @return 0 成功
 * @return <0失败
 */
extern "C" ZXVNMSSDK_EXPORT int __stdcall ZXVNMS_SetYUVCallback(
	void(__stdcall *fYUVCallback)(int handle,const char* data,int size,int width,int height,void* pUser),
	void* pUser);

/**
 * @brief 发送告警到平台
 *
 * @param[in] encoder_id		编码器ID
 * @param[in] channel			视频通道或DI端口
 * @param[in] alarmtype			告警类型
 * @param[in] alarmtime			格式:yyyy-mm-dd hh:mm:ss
 * @param[in] action			1：告警产生 0：告警消除
 *
 * @return 0 成功
 * @return <0失败
 */
extern "C" ZXVNMSSDK_EXPORT int __stdcall ZXVNMS_SendAlarmToCMS(
	const char* encoder_id,
	int channel,
	int alarmtype,
	const char* alarmtime,
	int action);

/**
 * @brief 设置代理转发服务器
 *
 * @param[in] proxydispatch_ip		代理转发服务器IP
 * @param[in] proxydispatch_port	代理转发服务器PORT
 * @param[in] proxyenable			是否使用代理转发服务器：0—不使用，1—使用
 *
 * @return 0 成功
 * @return <0失败
 */
extern "C" ZXVNMSSDK_EXPORT int __stdcall ZXVNMS_SetProxyDispatch(
	const char* proxydispatch_ip,
	int proxydispatch_port,
	int proxyenable);

/**
 * @brief 获取代理转发服务器
 *
 * @param[out] proxydispatch_ip		代理转发服务器IP
 * @param[out] proxydispatch_port	代理转发服务器PORT
 * @param[out] proxyenable			是否使用代理转发服务器：0—不使用，1—使用
 *
 * @return 0 成功
 * @return <0失败
 */
extern "C" ZXVNMSSDK_EXPORT int __stdcall ZXVNMS_GetProxyDispatch(
	char* proxydispatch_ip,
	int* proxydispatch_port,
	int* proxyenable);

/**
 * @brief 查询视频rtsp链接
 *
 * @param[in] param	预留参数
 *
 * @return 0 成功
 * @return <0失败
 */
extern "C" ZXVNMSSDK_EXPORT int __stdcall ZXVNMS_QueryRtspUrl(int param);

#endif

