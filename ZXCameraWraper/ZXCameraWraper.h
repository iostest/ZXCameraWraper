#pragma once
#define UNMANAGEDDLL_EXPORTS

#ifdef UNMANAGEDDLL_EXPORTS
#define UNMANAGEDDLL_API __declspec(dllexport)
#else
#define UNMANAGEDDLL_API __declspec(dllimport)
#endif

#define CHAR_LEN 500

typedef struct {
	char cameraId[CHAR_LEN];
	char cameraName[CHAR_LEN];
} CameraItem;


#ifdef __cplusplus
extern "C" {
#endif

	extern UNMANAGEDDLL_API	int GetCameras(const char* server, int port, const char* userName, const char* pwd, int startIndex,
		int maxCameraCount, CameraItem* cameras);

	extern UNMANAGEDDLL_API	int PlayVideo(const char* cameraID, long hWnd);

	extern UNMANAGEDDLL_API bool YV12ToBGR24_OpenCV(unsigned char* pYUV, unsigned char* pBGR24, int width, int height,int pBGR24BufferLen);

#ifdef __cplusplus
}
#endif
